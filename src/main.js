var video
var poseNet
var pose
var skeleton
var font
var boardSize
var states = {
  welcome: true,
  guesses: false,
  guessed: false
}
var texts = {
  welcome: 'this is a webcam application for checking time   ···   start it by clicking into the cirlce   ···   ',
  guesses: 'guess the current time using your hands    ···    guess the current time using your hands    ···    ',
  guess1: '00:00',
  guess2: '00:00',
  guessed: 'it is correct, the current time is ',
  guess: null,
  temperature: ['freezing', 'cold', 'chilly', 'cool', 'warm', 'hot']
}
var tempColors = [
  [16, 52, 166],
  [65, 47, 136],
  [114, 43, 106],
  [162, 38, 75],
  [211, 33, 45],
  [246, 45, 45]
]
var size = {
  x: 640,
  y: 480
}
var sWeight = 4
var currentDate
var dateTime
var a1, a2
var guess1 = {
  h: null,
  m: null
}
var guess2 = {
  h: null,
  m: null
}
var currentTime = {
  h: null,
  m: null
}
var accuracy = 0
var previousTime = null
var temperature = 0.0

function preload() {
  font = loadFont('ttf/UbuntuMono-Bold.ttf')
}

function setup() {
  createCanvas(windowWidth, windowHeight)
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  video = createCapture(VIDEO)
  video.hide()
  poseNet = ml5.poseNet(video)
  poseNet.on('pose', gotPoses)
  currentDate = new Date()
  dateTime = [currentDate.getHours(), currentDate.getMinutes()]
  if (dateTime[0] > 12) {
    dateTime[0] -= 12
  }
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  colorMode(RGB, 255, 255, 255, 1)
  rectMode(CENTER)
  imageMode(CENTER)
  textFont(font)
  textAlign(CENTER, CENTER)
  textSize(24)
  background(0)
  if (states.welcome === true) {
    push()
    translate(windowWidth * 0.5, windowHeight * 0.5)
    if (dist(mouseX, mouseY, windowWidth * 0.5, windowHeight * 0.5) <= (size.y + sWeight) * 0.5) {
      fill(255)
    } else {
      noFill()
    }
    stroke(255)
    strokeWeight(sWeight)
    ellipse(0, 0, size.y)
    for (var i = 0; i < texts.welcome.length; i++) {
      push()
      translate(-sin(Math.PI * 2 * (i / texts.welcome.length) + Math.PI - frameCount * 0.005) * 270, cos(Math.PI * 2 * (i / texts.welcome.length) + Math.PI - frameCount * 0.005) * 270)
      push()
      rotate(Math.PI * 2 * (i / texts.welcome.length) - frameCount * 0.005)
      fill(255)
      noStroke()
      text(texts.welcome.split('')[i], 0, 0)
      pop()
      pop()
    }
    pop()
  }
  if (states.guesses === true) {
    push()
    translate(windowWidth * 0.5, windowHeight * 0.5)
    push()
    scale(-1, 1)
    image(video, 0, 0)
    filter(GRAY)
    pop()
    noFill()
    stroke(0)
    strokeWeight(400)
    ellipse(0, 0, 640 + 160)
    noFill()
    stroke(255)
    strokeWeight(sWeight)
    ellipse(0, 0, 480)
    for (var i = 0; i < texts.guesses.length; i++) {
      push()
      translate(-sin(Math.PI * 2 * (i / texts.guesses.length) + Math.PI - frameCount * 0.005) * 270, cos(Math.PI * 2 * (i / texts.guesses.length) + Math.PI - frameCount * 0.005) * 270)
      push()
      rotate(Math.PI * 2 * (i / texts.guesses.length) - frameCount * 0.005)
      fill(255)
      noStroke()
      text(texts.guesses.split('')[i], 0, 0)
      pop()
      pop()
    }
    for (var i = 0; i < 60; i++) {
      push()
      translate(-sin(Math.PI * 2 * (i / 60) + Math.PI) * 240, cos(Math.PI * 2 * (i / 60) + Math.PI) * 240)
      push()
      rotate(Math.PI * 2 * (i / 60))
      noFill()
      stroke(255)
      strokeWeight(sWeight)
      if (i % 5 === 0) {
        line(0, 0, 0, 24)
      } else {
        line(0, 0, 0, 8)
      }
      pop()
      pop()
    }
    pop()
    if (pose) {
      push()
      translate((windowWidth + video.width) * 0.5, (windowHeight - video.height) * 0.5)
      scale(-1, 1)
      let earR = pose.rightEar
      let earL = pose.leftEar
      let d = dist(earR.x, earR.y, earL.x, earL.y)
      noFill()
      stroke(255)
      strokeWeight(sWeight)
      ellipse(pose.nose.x, pose.nose.y, d)
      fill(255)
      drawArrow(pose.rightWrist.x, pose.rightWrist.y, pose.rightElbow.x, pose.rightElbow.y)
      drawArrow(pose.leftWrist.x, pose.leftWrist.y, pose.leftElbow.x, pose.leftElbow.y)
      line(pose.leftShoulder.x, pose.leftShoulder.y, pose.rightShoulder.x, pose.rightShoulder.y)
      line(pose.leftHip.x, pose.leftHip.y, pose.rightHip.x, pose.rightHip.y)
      line(pose.leftShoulder.x, pose.leftShoulder.y, pose.leftHip.x, pose.leftHip.y)
      line(pose.rightShoulder.x, pose.rightShoulder.y, pose.rightHip.x, pose.rightHip.y)
      line(pose.rightShoulder.x, pose.rightShoulder.y, pose.rightElbow.x, pose.rightElbow.y)
      line(pose.rightElbow.x, pose.rightElbow.y, pose.rightWrist.x, pose.rightWrist.y)
      line(pose.leftShoulder.x, pose.leftShoulder.y, pose.leftElbow.x, pose.leftElbow.y)
      line(pose.leftElbow.x, pose.leftElbow.y, pose.leftWrist.x, pose.leftWrist.y)
      line(pose.rightHip.x, pose.rightHip.y, pose.rightKnee.x, pose.rightKnee.y)
      line(pose.rightKnee.x, pose.rightKnee.y, pose.rightAnkle.x, pose.rightAnkle.y)
      line(pose.leftHip.x, pose.leftHip.y, pose.leftKnee.x, pose.leftKnee.y)
      line(pose.leftKnee.x, pose.leftKnee.y, pose.leftAnkle.x, pose.leftAnkle.y)
      pop()
      push()
      a1 = atan2(pose.rightElbow.y - pose.rightWrist.y, pose.rightElbow.x - pose.rightWrist.x)
      translate(windowWidth * 0.5 - sin(-a1 - Math.PI * 0.5) * 240, windowHeight * 0.5 + cos(-a1 - Math.PI * 0.5) * 240)
      fill(0)
      stroke(255)
      strokeWeight(sWeight)
      ellipse(0, 0, 24)
      pop()
      push()
      a2 = atan2(pose.leftElbow.y - pose.leftWrist.y, pose.leftElbow.x - pose.leftWrist.x)
      translate(windowWidth * 0.5 - sin(-a2 - Math.PI * 0.5) * 240, windowHeight * 0.5 + cos(-a2 - Math.PI * 0.5) * 240)
      fill(0)
      stroke(255)
      strokeWeight(sWeight)
      ellipse(0, 0, 24)
      pop()
      guess1.h = Math.round((-a2 + Math.PI * 0.5) / (Math.PI * 2) * 12)
      guess1.m = Math.floor((-a1 + Math.PI * 0.5) / (Math.PI * 2) * 60)
      if (guess1.h < 0) {
        guess1.h += 12
      }
      if (guess1.m < 0) {
        guess1.m += 60
      }
      if (guess1.h < 10) {
        guess1.h = '0' + parseInt(guess1.h)
      }
      if (guess1.m < 10) {
        guess1.m = '0' + parseInt(guess1.m)
      }
      guess2.h = Math.round((-a1 + Math.PI * 0.5) / (Math.PI * 2) * 12)
      guess2.m = Math.floor((-a2 + Math.PI * 0.5) / (Math.PI * 2) * 60)
      if (guess2.h < 0) {
        guess2.h += 12
      }
      if (guess2.m < 0) {
        guess2.m += 60
      }
      if (guess2.h < 10) {
        guess2.h = '0' + parseInt(guess2.h)
      }
      if (guess2.m < 10) {
        guess2.m = '0' + parseInt(guess2.m)
      }
      texts.guess1 = guess1.h + ':' + guess1.m
      texts.guess2 = guess2.h + ':' + guess2.m
      if (currentTime.h < 10) {
        currentTime.h = '0' + parseInt(currentTime.h)
      }
      if (currentTime.m < 10) {
        currentTime.m = '0' + parseInt(currentTime.m)
      }
      if (currentTime.h === guess1.h && dist(0, currentTime.m, 0, parseInt(guess1.m)) <= accuracy || currentTime.h === guess2.h && dist(0, currentTime.m, 0, parseInt(guess2.m)) <= accuracy) {
        states.guesses = false
        states.guessed = true
        previousTime = currentTime.h + ':' + parseInt(parseInt(currentTime.m) + 1)
      }
    }
    push()
    translate(windowWidth * 0.5, windowHeight * 0.5)
    noFill()
    stroke(255)
    strokeWeight(sWeight * 5)
    arc(0, 0, 620, 620, -Math.PI * 1.25, Math.PI * 0.25)
    stroke(tempColors[Math.floor(temperature * 6)])
    strokeWeight(sWeight * 2.5)
    arc(0, 0, 620, 620, -Math.PI * 1.25, -Math.PI * 1.249 + temperature * Math.PI * 1.5)
    for (var i = 0; i < texts.temperature[Math.floor(temperature * 6)].length; i++) {
      push()
      translate(-sin(Math.PI * (0.035 * texts.temperature[Math.floor(temperature * 6)].length) * (i / texts.temperature[Math.floor(temperature * 6)].length) - 0.035 * texts.temperature[Math.floor(temperature * 6)].length * 1.25) * 305, cos(Math.PI * (0.035 * texts.temperature[Math.floor(temperature * 6)].length) * (i / texts.temperature[Math.floor(temperature * 6)].length) - 0.035 * texts.temperature[Math.floor(temperature * 6)].length * 1.25) * 305)
      push()
      rotate(Math.PI * (0.035 * texts.temperature[Math.floor(temperature * 6)].length) * (i / texts.temperature[Math.floor(temperature * 6)].length) - 0.035 * texts.temperature[Math.floor(temperature * 6)].length * 1.25)
      fill(255)
      noStroke()
      textSize(64)
      text(texts.temperature[Math.floor(temperature * 6)].split("").reverse().join("").split('')[i], 0, 0)
      pop()
      pop()
    }
    temperature = Math.max(1 - Math.abs(new Date(2021, 05, 20, dateTime[0], dateTime[1], 0, 0) - new Date(2021, 05, 20, parseInt(texts.guess1.slice(0, 2)), parseInt(texts.guess1.slice(3, 5)), 0, 0)) / (60 * 60 * 1000) / 13, 1 - Math.abs(new Date(2021, 05, 20, dateTime[0], dateTime[1], 0, 0) - new Date(2021, 05, 20, parseInt(texts.guess1.slice(0, 2)), parseInt(texts.guess1.slice(3, 5)), 0, 0)) / (60 * 60 * 1000) / 13)
    pop()
  }
  if (states.guessed === true) {
    if (currentTime.h < 10) {
      currentTime.h = '0' + parseInt(currentTime.h)
    }
    if (currentTime.m < 10) {
      currentTime.m = '0' + parseInt(currentTime.m)
    }
    texts.guess = texts.guessed + new Date().getHours() + ':' + currentTime.m + '   ···   ' + texts.guessed + new Date().getHours() + ':' + currentTime.m + '   ···   '
    push()
    translate(windowWidth * 0.5, windowHeight * 0.5)
    push()
    scale(-1, 1)
    image(video, 0, 0)
    filter(GRAY)
    pop()
    noFill()
    stroke(0)
    strokeWeight(400)
    ellipse(0, 0, 640 + 160)
    noFill()
    stroke(255)
    strokeWeight(sWeight)
    ellipse(0, 0, 480)
    for (var i = 0; i < 60; i++) {
      push()
      translate(-sin(Math.PI * 2 * (i / 60) + Math.PI) * 240, cos(Math.PI * 2 * (i / 60) + Math.PI) * 240)
      push()
      rotate(Math.PI * 2 * (i / 60))
      if (i % 5 === 0) {
        line(0, 0, 0, 24)
      } else {
        line(0, 0, 0, 8)
      }
      pop()
      pop()
    }
    noFill()
    stroke(255)
    strokeWeight(sWeight)
    line(0, 0, -sin(Math.PI * 2 * ((5 * dateTime[0]) / 60) + Math.PI + (dateTime[1] / (120))) * 120, cos(Math.PI * 2 * ((5 * dateTime[0]) / 60) + Math.PI + (dateTime[1] / (120))) * 120)
    line(0, 0, -sin(Math.PI * 2 * (dateTime[1] / 60) + Math.PI) * 200, cos(Math.PI * 2 * (dateTime[1] / 60) + Math.PI) * 200)
    for (var i = 0; i < texts.guess.length; i++) {
      push()
      translate(-sin(Math.PI * 2 * (i / texts.guess.length) + Math.PI - frameCount * 0.005) * 270, cos(Math.PI * 2 * (i / texts.guess.length) + Math.PI - frameCount * 0.005) * 270)
      push()
      rotate(Math.PI * 2 * (i / texts.guess.length) - frameCount * 0.005)
      fill(255)
      noStroke()
      text(texts.guess.split('')[i], 0, 0)
      pop()
      pop()
    }
    pop()
    if (previousTime === currentTime.h + ':' + currentTime.m) {
      states.guessed = false
      states.welcome = true
    }
  }
  currentDate = new Date()
  dateTime = [currentDate.getHours(), currentDate.getMinutes()]
  if (dateTime[0] > 12) {
    dateTime[0] -= 12
  }
  currentTime.h = dateTime[0]
  currentTime.m = dateTime[1]
}

function mousePressed() {
  if (states.welcome === true) {
    if (dist(mouseX, mouseY, windowWidth * 0.5, windowHeight * 0.5) <= (size.y + sWeight) * 0.5) {
      states.welcome = false
      states.guesses = true
    }
  }
}

function windowResized() {
  createCanvas(windowWidth, windowHeight)
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
}

function drawArrow(x1, y1, x2, y2) {
  var size = dist(x1, y1, x2, y2)
  var angle = atan2(y2 - y1, x2 - x1) + Math.PI * 0.5
  push()
  translate(x1, y1)
  rotate(angle)
  beginShape()
  vertex(0, 0)
  vertex(-size * 0.125, -size * 0.35)
  vertex(size * 0.125, -size * 0.35)
  endShape(CLOSE)
  pop()
}

function gotPoses(poses) {
  if (poses.length > 0) {
    pose = poses[0].pose
    skeleton = poses[0].skeleton
  }
}
